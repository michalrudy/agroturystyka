# Gospodarstwo Agroturystyczne "Na Górze"
![](screenshots/logo.png)

* Informacje ogólne
* Opis aplikacji
* Kryteria ewaluacji projektu
* Technologie
* Widoki

## Informacje ogólne
Aplikacja Agroturystyka "Na Górze" umożliwia przeglądanie oferty noclegowej obiektu. Pozwala ona na założenie osobistego konta, dzięki któremu użytkownik jest w stanie zarezerwować konkretny pokój w wybranym przez siebie terminie. Użytkownik ma możliwość uzupełnienia dodatkowych informacji, zmianę zdjęcia profilowego, zmianę hasła oraz wyświetlenie listy utworzony rezerwacji. Responsywny rozmiar stron pozwala na wygodą pracę na każdym urządzeniu.

## Opis aplikacji
Strona główna, zaprojektowana w stylu 'one page design', zawiera opis obiektu oraz ofertę noclegowa. Na początku strony znajduje się pozioma nawigacja, która umożliwia płynne przełączanie się między poszczególnymi sekcjami strony. Przejście na np. drugą sekcję strony powoduje wyświetlenie w prawym dolnym rogu ekranu strzałki pozwalającej wrócić do początku. Ikona usera pozwala na przejście do profilu użytkownika, rozwijana lista pozwala na zarezerwowanie pokoju lub zalogowanie się do serwisu. Wszystkie widoki aplikacji dopasowują się do rozmiaru ekranu użytkownika.
* Na stronie logowania znajduje się formularz zawierający pola e-mail oraz hasło do zalogowania, jest również przycisk zaloguj. Dodatkowo użytkownik jest w stanie utworzyć konto klikając na zarejestruj się lub w przypadku administratora, przejście do panelu logowania administratora. Niepoprawny proces logowania powoduje wyświetlenie informacji o błędzie.
* Widok rejestracji zawiera formularz zakładania konta z polami e-mail, hasło, Imię, Nazwisko, numer telefonu, akceptacja regulaminu oraz przycisk zarejestruj się. Poprawny proces rejestracji skutkuje wyświetleniem odpowiednej informacji jak i w przypadku błędu.
* Po wyświetleniu strony profilu, użytkownik ma możliwość edycji własnych danych. W zakładce 'moje dane' znajdują się dane o użytkowniku, które zostały pobrane z bazy danych. Zakładka 'rezerwacje' umożliwia podgląd złożonych rezerwacji pokoi oraz wszystkich informacji związanych z operacją. 'Zdjęcie profilowe' pozwala na zmianę lub dodanie zdjęcia profilowego. Zakładka 'hasło' umożliwia zmianę hasła na nowe.
* Strona procesu rezerwacji zawiera kafelki z dostępnymi pokojami w obiekcie oraz ich informacje. Następnie użytkownik wybiera datę przyjazdu i datę wyjazdu oraz jest możliwość dołączenia dodatkowych informacji. Formularz kończy przycisk 'zarezerwuj', który przenosi użytkownika do strony z potwierdzaniem rezerwacji oraz wypisanie wszystkich podanych informacji.
* Wylogowywanie użytkownika przenosi go z powrotem do strony logowania.
* Widok panelu administratora zawiera dwie zakładki. Pierwsza z nich 'konta użytkowników' zawiera listę wszystkich użytkowników w systemie. Administrator ma możliwość usuwania wybranych kont poprzez kliknięcie w przycisk usuń konto. Zakładka 'zmiana hasła' umożliwia administratorowi zmianę hasła na nowe.
* Wszystkie formularze zostały zabezpieczone mechanizmami blokującymi nieporządane sytuacje.

## Kryteria ewaluacji projektu

* [x] 1.DOKUMENTACJE W README.MD
* [x] 2.KOD NAPISANY OBIEKTOWO (CZĘŚĆ BACKENDOWA)
* [x] 3.DIAGRAM ERD
* [x] 4.GIT
* [x] 5.REALIZACJA TEMATU
* [x] 6.HTML
* [x] 7.POSTGRESQL
* [x] 8.ZŁOŻONOŚĆ BAZY DANYCH
* [x] 9.PHP
* [x] 10.JAVA SCRIPT
* [x] 11.FETCH API (AJAX)
* [x] 12.DESIGN
* [x] 13.RESPONSYWNOŚĆ
* [x] 14.LOGOWANIE
* [x] 15.SESJA UŻYTKOWNIKA
* [x] 16.UPRAWNIENIA UŻYTKOWNIKÓW
* [x] 17.ROLE UŻYTKOWNIKÓW
* [x] 18.WYLOGOWYWANIE
* [x] 19.WIDOKI, WYZWALACZ/ FUNKCJE, TRANSAKCJE NA BAZIE DANYCH
* [x] 20.AKCJE NA REFERENCJACH
* [x] 21.BEZPIECZEŃSTWO
* [x] 22.BRAK REPLIKACJI KODU
* [x] 23.CZYSTOŚĆ I PRZEJRZYSTOŚĆ KODU
* [x] 24.BAZA DANYCH ZRZUCONA DO PLIKU .SQL

## Diagram EDR
### Diagram zaprojektowany w zewnętrznej apliacji
![](screenshots/ERD.png)
### Diagram zaprojektowany w PHP Storm
![](screenshots/ERD-storm.png)

## Technologie
* Html
* Css
* JavaScript
* PostgreSQL
* PHP version 8.0
* Nginx version 1.17.8-alpine

## Widoki

* Strona główna
  ![](screenshots/desktop-view-1.png)
* Logowanie
  ![](screenshots/desktop-view-2.png)
* Rejestracja
  ![](screenshots/desktop-view-3.png)
* Profil użytkownika
  ![](screenshots/desktop-view-4.png)
* Rezerwacje użytkownika
  ![](screenshots/desktop-view-5.png)
* Formularz rezerwacji
  ![](screenshots/desktop-view-6.png)
* Formularz rezerwacji cd.
  ![](screenshots/desktop-view-7.png)
* Panel administracyjny
  ![](screenshots/desktop-view-8.png)
* Panel administracyjny - zarządzanie użytkownikami 
  ![](screenshots/desktop-view-9.png)


* Strona główna
![](screenshots/mobile-view-1.png)
* Profil użytkownika
![](screenshots/mobile-view-2.png)
* Profil użytkownika cd.
![](screenshots/mobile-view-3.png)
* Zdjęcie profilowe
![](screenshots/mobile-view-4.png)
* Dane użytkownika
![](screenshots/mobile-view-5.png)
* Rezerwacje użytkownika
![](screenshots/mobile-view-6.png)
* Zmiana hasła
![](screenshots/mobile-view-7.png)
* Formularz rezerwacji pokoju
![](screenshots/mobile-view-8.png)
* Formularz rezerwacji pokoju cd.
![](screenshots/mobile-view-9.png)
* Zmiana hasła administratora
![](screenshots/mobile-view-10.png)
* Lista wszystkich użytkowników
![](screenshots/mobile-view-11.png)