create table account_type
(
    "ID_account_type" serial
        constraint account_type_pk
            primary key,
    account_name      varchar(50) not null,
    ts_account_type   timestamp default CURRENT_TIMESTAMP
);

create unique index account_type_account_name_uindex
    on account_type (account_name);

create unique index account_type_id_account_type_uindex
    on account_type ("ID_account_type");

INSERT INTO public.account_type ("ID_account_type", account_name, ts_account_type) VALUES (1, 'użytkownik', '2021-12-24 20:13:54.385433');
INSERT INTO public.account_type ("ID_account_type", account_name, ts_account_type) VALUES (2, 'administrator', '2021-12-24 20:14:49.964928');
INSERT INTO public.account_type ("ID_account_type", account_name, ts_account_type) VALUES (3, 'pracownik', '2021-12-24 20:14:49.964928');



create table booking
(
    "ID_booking"    serial
        constraint booking_pk
            primary key,
    "ID_users"      integer not null
        constraint booking_users_id_users_fk
            references users
            on update cascade on delete restrict,
    "ID_rooms"      integer not null
        constraint booking_rooms_id_rooms_fk
            references rooms
            on update cascade on delete restrict,
    arrival_date    date    not null,
    departure_date  date    not null,
    additional_info text,
    ts_booking      timestamp default CURRENT_TIMESTAMP
);

create unique index booking_id_booking_uindex
    on booking ("ID_booking");

INSERT INTO public.booking ("ID_booking", "ID_users", "ID_rooms", arrival_date, departure_date, additional_info, ts_booking) VALUES (12621, 32, 2, '2022-01-30', '2022-02-03', 'Proszę o dodatkowy ręcznik.', '2022-01-28 17:58:08.051709');
INSERT INTO public.booking ("ID_booking", "ID_users", "ID_rooms", arrival_date, departure_date, additional_info, ts_booking) VALUES (12622, 32, 1, '2022-01-30', '2022-02-03', 'Proszę o dodatkowy ręcznik.', '2022-01-28 17:58:08.517497');
INSERT INTO public.booking ("ID_booking", "ID_users", "ID_rooms", arrival_date, departure_date, additional_info, ts_booking) VALUES (12623, 14, 2, '2022-02-05', '2022-02-13', 'Proszę o dodatkową pościel.', '2022-01-28 18:00:10.236342');
INSERT INTO public.booking ("ID_booking", "ID_users", "ID_rooms", arrival_date, departure_date, additional_info, ts_booking) VALUES (12624, 34, 3, '2022-02-05', '2022-02-08', 'Proszę o dostawkę do łóżka.', '2022-01-28 18:03:28.520702');
INSERT INTO public.booking ("ID_booking", "ID_users", "ID_rooms", arrival_date, departure_date, additional_info, ts_booking) VALUES (12625, 34, 1, '2022-02-05', '2022-02-08', 'Proszę o dostawkę do łóżka.', '2022-01-28 18:03:28.960822');
INSERT INTO public.booking ("ID_booking", "ID_users", "ID_rooms", arrival_date, departure_date, additional_info, ts_booking) VALUES (12626, 35, 2, '2022-03-01', '2022-03-10', '', '2022-01-28 18:05:31.517447');
INSERT INTO public.booking ("ID_booking", "ID_users", "ID_rooms", arrival_date, departure_date, additional_info, ts_booking) VALUES (12627, 35, 2, '2022-02-10', '2022-02-20', 'Proszę o dodatkowe klucze do drzwi.', '2022-01-28 18:06:19.390547');
INSERT INTO public.booking ("ID_booking", "ID_users", "ID_rooms", arrival_date, departure_date, additional_info, ts_booking) VALUES (12628, 35, 3, '2022-02-10', '2022-02-20', 'Proszę o dodatkowe klucze do drzwi.', '2022-01-28 18:06:19.855931');
INSERT INTO public.booking ("ID_booking", "ID_users", "ID_rooms", arrival_date, departure_date, additional_info, ts_booking) VALUES (12629, 35, 2, '2022-05-02', '2022-05-04', '', '2022-01-28 18:06:41.770688');
INSERT INTO public.booking ("ID_booking", "ID_users", "ID_rooms", arrival_date, departure_date, additional_info, ts_booking) VALUES (12630, 35, 1, '2022-05-02', '2022-05-04', '', '2022-01-28 18:06:42.418104');
INSERT INTO public.booking ("ID_booking", "ID_users", "ID_rooms", arrival_date, departure_date, additional_info, ts_booking) VALUES (12631, 36, 1, '2022-02-01', '2022-02-06', '', '2022-01-28 18:08:09.857339');
INSERT INTO public.booking ("ID_booking", "ID_users", "ID_rooms", arrival_date, departure_date, additional_info, ts_booking) VALUES (12632, 36, 2, '2022-03-10', '2022-03-12', 'Pojawimy się około 23:30.', '2022-01-28 18:09:09.585601');



create table photos
(
    "ID_photos" serial
        constraint photos_pk
            primary key,
    photo       varchar(50),
    ts_photos   timestamp default CURRENT_TIMESTAMP
);

create unique index photos_id_photos_uindex
    on photos ("ID_photos");

create unique index photos_photo_uindex
    on photos (photo);

INSERT INTO public.photos ("ID_photos", photo, ts_photos) VALUES (3, 'trzyosobowy.jpg', '2022-01-04 17:36:43.867683');
INSERT INTO public.photos ("ID_photos", photo, ts_photos) VALUES (2, 'dwuosobowy.jpg', '2021-12-25 17:01:05.797041');
INSERT INTO public.photos ("ID_photos", photo, ts_photos) VALUES (4, 'szescioosobowy.jpg', '2022-01-05 11:10:37.671789');
INSERT INTO public.photos ("ID_photos", photo, ts_photos) VALUES (5, 'dwuosobowy2.jpg', '2022-01-05 11:40:30.926235');



create table rooms_photos
(
    "ID_rooms"      integer not null
        constraint rooms_photos_rooms_id_rooms_fk
            references rooms
            on update cascade on delete restrict,
    "ID_photos"     integer not null
        constraint rooms_photos_photos_id_photos_fk
            references photos,
    ts_rooms_photos timestamp default CURRENT_TIMESTAMP
);

INSERT INTO public.rooms_photos ("ID_rooms", "ID_photos", ts_rooms_photos) VALUES (1, 3, '2021-12-25 17:03:00.825336');
INSERT INTO public.rooms_photos ("ID_rooms", "ID_photos", ts_rooms_photos) VALUES (2, 2, '2022-01-04 17:37:05.345730');
INSERT INTO public.rooms_photos ("ID_rooms", "ID_photos", ts_rooms_photos) VALUES (3, 4, '2022-01-05 11:12:27.038984');
INSERT INTO public.rooms_photos ("ID_rooms", "ID_photos", ts_rooms_photos) VALUES (2, 5, '2022-01-05 11:41:21.271396');



create table rooms
(
    "ID_rooms"             serial
        constraint rooms_pk
            primary key,
    name                   varchar(30) not null,
    room_number            integer     not null,
    number_of_participants integer     not null,
    facilities             text,
    price                  numeric     not null,
    description            text,
    ts_rooms               timestamp default CURRENT_TIMESTAMP
);

create unique index rooms_id_rooms_uindex
    on rooms ("ID_rooms");

create unique index rooms_room_number_uindex
    on rooms (room_number);

INSERT INTO public.rooms ("ID_rooms", name, room_number, number_of_participants, facilities, price, description, ts_rooms) VALUES (1, 'trzyosobowy', 3, 3, null, 180, 'Mauris et odio facilisis, imperdiet nisl vitae, porttitor sem. Vivamus at nulla sed nulla iaculis 
hendrerit. Integer pulvinar eleifend nisl, et scelerisque ex venenatis non. Quisque semper 
scelerisque nulla non mollis.', '2022-01-04 15:06:20.242249');
INSERT INTO public.rooms ("ID_rooms", name, room_number, number_of_participants, facilities, price, description, ts_rooms) VALUES (2, 'dwuosobowy', 5, 2, null, 120, 'Mauris et odio facilisis, imperdiet nisl vitae, porttitor sem. Vivamus at nulla sed nulla iaculis 
hendrerit. Integer pulvinar eleifend nisl, et scelerisque ex venenatis non. Quisque semper 
scelerisque nulla non mollis.', '2022-01-04 15:13:16.818138');
INSERT INTO public.rooms ("ID_rooms", name, room_number, number_of_participants, facilities, price, description, ts_rooms) VALUES (3, 'sześcioosobowy', 2, 6, null, 360, 'Mauris et odio facilisis, imperdiet nisl vitae, porttitor sem. Vivamus at nulla sed nulla iaculis 
hendrerit. Integer pulvinar eleifend nisl, et scelerisque ex venenatis non. Quisque semper 
scelerisque nulla non mollis.', '2022-01-04 15:13:16.818138');



create table users_account_type
(
    "ID_users"            integer
        constraint users_account_type_users_id_users_fk
            references users
            on update cascade on delete restrict,
    "ID_account_type"     integer
        constraint users_account_type_account_type_id_account_type_fk
            references account_type
            on update cascade on delete restrict,
    ts_users_account_type timestamp default CURRENT_TIMESTAMP
);

create unique index users_account_type_id_users_uindex
    on users_account_type ("ID_users");

INSERT INTO public.users_account_type ("ID_users", "ID_account_type", ts_users_account_type) VALUES (14, 1, '2022-01-20 15:10:25.066269');
INSERT INTO public.users_account_type ("ID_users", "ID_account_type", ts_users_account_type) VALUES (23, 2, '2022-01-20 18:53:12.747049');
INSERT INTO public.users_account_type ("ID_users", "ID_account_type", ts_users_account_type) VALUES (32, 1, '2022-01-27 18:51:39.055938');
INSERT INTO public.users_account_type ("ID_users", "ID_account_type", ts_users_account_type) VALUES (34, 1, '2022-01-28 17:45:34.105964');
INSERT INTO public.users_account_type ("ID_users", "ID_account_type", ts_users_account_type) VALUES (35, 1, '2022-01-28 17:46:15.272421');
INSERT INTO public.users_account_type ("ID_users", "ID_account_type", ts_users_account_type) VALUES (36, 1, '2022-01-28 17:47:20.736592');



create table users_details
(
    "ID_users_details" serial
        constraint users_details_pk
            primary key,
    name               varchar(50) not null,
    surname            varchar(50) not null,
    street             varchar(30),
    street_number      integer,
    postal_code        varchar(30),
    city               varchar(20),
    phon_number        varchar(50) not null,
    profile_picture    varchar(50),
    ts_users_details   timestamp default CURRENT_TIMESTAMP
);

create unique index users_details_id_users_details_uindex
    on users_details ("ID_users_details");

INSERT INTO public.users_details ("ID_users_details", name, surname, street, street_number, postal_code, city, phon_number, profile_picture, ts_users_details) VALUES (44, 'Roman', 'Walczak', 'Warszawska', 27, '32-345', 'Kraków', '+5812345678', 'rwalczak_profile.jpeg', '2021-12-31 13:02:10.574397');
INSERT INTO public.users_details ("ID_users_details", name, surname, street, street_number, postal_code, city, phon_number, profile_picture, ts_users_details) VALUES (57, 'Jędrzej', 'Łukasik', 'Parkowa', 556, '52-830', 'Rybnik', '+84123456789', '1000_F_167891864.jpeg', '2022-01-27 18:51:37.620889');
INSERT INTO public.users_details ("ID_users_details", name, surname, street, street_number, postal_code, city, phon_number, profile_picture, ts_users_details) VALUES (59, 'Agata', 'Maj', 'Piastowska', 17, '45-803', 'Szczecin', '+78123456789', '73413d8a60b2f59f72992.jpeg', '2022-01-28 17:45:32.402149');
INSERT INTO public.users_details ("ID_users_details", name, surname, street, street_number, postal_code, city, phon_number, profile_picture, ts_users_details) VALUES (60, 'Kajetan', 'Żak', 'Orzeszkowej', 369, '20-412', 'Katowice', '+09987654321', 'ikona-profilu-zdjecie.jpeg', '2022-01-28 17:46:13.895150');
INSERT INTO public.users_details ("ID_users_details", name, surname, street, street_number, postal_code, city, phon_number, profile_picture, ts_users_details) VALUES (61, 'Teodozja', 'Głowacka', 'Konopnickiej', 4, '92-629', 'Szczecin', '+56123098746', 'images.jpeg', '2022-01-28 17:47:19.298911');



create table users
(
    "ID_users"         serial
        constraint users_pk
            primary key,
    email              varchar(100) not null,
    password           varchar(255) not null,
    enabled            boolean   default false,
    "ID_users_details" integer
        constraint users_users_details_id_users_details_fk
            references users_details
            on update cascade on delete restrict,
    ts_users           timestamp default CURRENT_TIMESTAMP
);

create unique index users_id_users_details_uindex
    on users ("ID_users_details");

create unique index users_id_users_uindex
    on users ("ID_users");

create unique index users_email_uindex
    on users (email);

INSERT INTO public.users ("ID_users", email, password, enabled, "ID_users_details", ts_users) VALUES (32, 'jlukasik@pk.edu.pl', '$2y$10$HOTXoSsYyyvdPcNrNTUpmuOWuToHtRpPrHy8fHt66HVJiJ4cgCfti', false, 57, '2022-01-27 18:51:37.620889');
INSERT INTO public.users ("ID_users", email, password, enabled, "ID_users_details", ts_users) VALUES (14, 'rwalczak@pk.edu.pl', '$2y$10$lShCqMIexZuVRGg70UmBO.ybClU9/KLQqdsD0LErImcjQIjdIcqGS', false, 44, '2021-12-31 13:02:10.574397');
INSERT INTO public.users ("ID_users", email, password, enabled, "ID_users_details", ts_users) VALUES (34, 'amaj@pk.edu.pl', '$2y$10$B5d2SqUNzz3SwWG0T2UkmuuK/RTb2pGpWBAfcnEdsnbL9wBighSQ.', false, 59, '2022-01-28 17:45:32.402149');
INSERT INTO public.users ("ID_users", email, password, enabled, "ID_users_details", ts_users) VALUES (35, 'kzak@pk.edu.pl', '$2y$10$XYYPdpYP2x1jtgn6pryiEOWbH6qRmoa.IvlY7xUmqy8TQYXZZiz1i', false, 60, '2022-01-28 17:46:13.895150');
INSERT INTO public.users ("ID_users", email, password, enabled, "ID_users_details", ts_users) VALUES (36, 'tglowacka@pk.edu.pl', '$2y$10$5iMOOL2B8cOePC.SVS6q3OyhT7Xr49BwbzFsG4rCPMyI6QpcoycPS', false, 61, '2022-01-28 17:47:19.298911');
INSERT INTO public.users ("ID_users", email, password, enabled, "ID_users_details", ts_users) VALUES (23, 'admin', '$2y$10$IyVX5FmvIGvf.H21VwfoH.1K9Xpa54jgioTZ7TWXj66iNyqC2x2nG', true, null, '2022-01-20 18:51:00.249424');



create view v_all_rooms(name, price, description, photo, "ID_rooms") as
SELECT DISTINCT ON (r.name) r.name,
                            r.price,
                            r.description,
                            t1.photo,
                            r."ID_rooms"
FROM (SELECT rp."ID_rooms",
             p.photo
      FROM photos p
               JOIN rooms_photos rp ON p."ID_photos" = rp."ID_photos") t1
         JOIN rooms r ON r."ID_rooms" = t1."ID_rooms"
ORDER BY r.name;

INSERT INTO public.v_all_rooms (name, price, description, photo, "ID_rooms") VALUES ('dwuosobowy', 120, 'Mauris et odio facilisis, imperdiet nisl vitae, porttitor sem. Vivamus at nulla sed nulla iaculis 
hendrerit. Integer pulvinar eleifend nisl, et scelerisque ex venenatis non. Quisque semper 
scelerisque nulla non mollis.', 'dwuosobowy.jpg', 2);
INSERT INTO public.v_all_rooms (name, price, description, photo, "ID_rooms") VALUES ('sześcioosobowy', 360, 'Mauris et odio facilisis, imperdiet nisl vitae, porttitor sem. Vivamus at nulla sed nulla iaculis 
hendrerit. Integer pulvinar eleifend nisl, et scelerisque ex venenatis non. Quisque semper 
scelerisque nulla non mollis.', 'szescioosobowy.jpg', 3);
INSERT INTO public.v_all_rooms (name, price, description, photo, "ID_rooms") VALUES ('trzyosobowy', 180, 'Mauris et odio facilisis, imperdiet nisl vitae, porttitor sem. Vivamus at nulla sed nulla iaculis 
hendrerit. Integer pulvinar eleifend nisl, et scelerisque ex venenatis non. Quisque semper 
scelerisque nulla non mollis.', 'trzyosobowy.jpg', 1);



create view v_all_users
            (name, surname, email, phon_number, street, street_number, postal_code, city, profile_picture,
             account_name) as
SELECT t2.name,
       t2.surname,
       t2.email,
       t2.phon_number,
       t2.street,
       t2.street_number,
       t2.postal_code,
       t2.city,
       t2.profile_picture,
       at.account_name
FROM (SELECT t1.name,
             t1.surname,
             t1.email,
             t1.phon_number,
             t1.street,
             t1.street_number,
             t1.postal_code,
             t1.city,
             t1.profile_picture,
             ua."ID_account_type"
      FROM (SELECT ud.name,
                   ud.surname,
                   u.email,
                   ud.phon_number,
                   ud.street,
                   ud.street_number,
                   ud.postal_code,
                   ud.city,
                   ud.profile_picture,
                   u."ID_users"
            FROM users u
                     LEFT JOIN users_details ud ON ud."ID_users_details" = u."ID_users_details") t1
               JOIN users_account_type ua ON ua."ID_users" = t1."ID_users") t2
         JOIN account_type at ON at."ID_account_type" = t2."ID_account_type";


INSERT INTO public.v_all_users (name, surname, email, phon_number, street, street_number, postal_code, city, profile_picture, account_name) VALUES ('Roman', 'Walczak', 'rwalczak@pk.edu.pl', '+5812345678', 'Warszawska', 27, '32-345', 'Kraków', 'rwalczak_profile.jpeg', 'użytkownik');
INSERT INTO public.v_all_users (name, surname, email, phon_number, street, street_number, postal_code, city, profile_picture, account_name) VALUES (null, null, 'admin', null, null, null, null, null, null, 'administrator');
INSERT INTO public.v_all_users (name, surname, email, phon_number, street, street_number, postal_code, city, profile_picture, account_name) VALUES ('Jędrzej', 'Łukasik', 'jlukasik@pk.edu.pl', '+84123456789', 'Parkowa', 556, '52-830', 'Rybnik', '1000_F_167891864.jpeg', 'użytkownik');
INSERT INTO public.v_all_users (name, surname, email, phon_number, street, street_number, postal_code, city, profile_picture, account_name) VALUES ('Agata', 'Maj', 'amaj@pk.edu.pl', '+78123456789', 'Piastowska', 17, '45-803', 'Szczecin', '73413d8a60b2f59f72992.jpeg', 'użytkownik');
INSERT INTO public.v_all_users (name, surname, email, phon_number, street, street_number, postal_code, city, profile_picture, account_name) VALUES ('Kajetan', 'Żak', 'kzak@pk.edu.pl', '+09987654321', 'Orzeszkowej', 369, '20-412', 'Katowice', 'ikona-profilu-zdjecie.jpeg', 'użytkownik');
INSERT INTO public.v_all_users (name, surname, email, phon_number, street, street_number, postal_code, city, profile_picture, account_name) VALUES ('Teodozja', 'Głowacka', 'tglowacka@pk.edu.pl', '+56123098746', 'Konopnickiej', 4, '92-629', 'Szczecin', 'images.jpeg', 'użytkownik');






create function check_book() returns trigger
	language plpgsql
as $$
DECLARE currentArrivalDate DATE;
    DECLARE currentDepartureDate DATE;
    DECLARE flag BOOLEAN;
    BEGIN
        IF (NEW."ID_rooms" IN (SELECT "ID_rooms" FROM booking))
        THEN
            FOR currentArrivalDate IN SELECT arrival_date FROM booking WHERE "ID_rooms" = NEW."ID_rooms" LOOP
                SELECT departure_date FROM booking WHERE arrival_date = currentArrivalDate INTO currentDepartureDate;
                IF ((NEW.arrival_date NOT BETWEEN currentArrivalDate AND currentDepartureDate AND NEW.departure_date < currentArrivalDate) OR (NEW.departure_date > currentArrivalDate AND NEW.arrival_date > currentDepartureDate))
                THEN
                    --INSERT INTO public.booking ("ID_booking", "ID_users", "ID_rooms", arrival_date, departure_date, additional_info, ts_booking) VALUES (DEFAULT, NEW."ID_users", NEW."ID_rooms", NEW.arrival_date, NEW.departure_date, NEW.additional_info, DEFAULT);
                    flag = TRUE;
                    --CALL insertData(NEW."ID_users", NEW."ID_rooms", NEW.arrival_date, NEW.departure_date, NEW.additional_info);
                    EXIT;
                    --RAISE EXCEPTION 'Rezerwacja dodana!';
                ELSE
                    RAISE EXCEPTION 'Pokój w tym terminie jest zajęty!';
                END IF;
            END LOOP;
            IF (flag IS TRUE)
            THEN
                INSERT INTO booking ("ID_users", "ID_rooms", arrival_date, departure_date, additional_info) VALUES (NEW."ID_users", NEW."ID_rooms", NEW.arrival_date, NEW.departure_date, NEW.additional_info);
            END IF;
        ELSE
            INSERT INTO booking ("ID_users", "ID_rooms", arrival_date, departure_date, additional_info) VALUES (NEW."ID_users", NEW."ID_rooms", NEW.arrival_date, NEW.departure_date, NEW.additional_info);
        END IF;
        RETURN NEW;
    --END IF;
    END;
$$;




create procedure insertdata(idusers integer, idrooms integer, arrivaldate date, departuredate date, additionalinfo text)
	language sql
as $$
INSERT INTO public.booking ("ID_users", "ID_rooms", arrival_date, departure_date, additional_info) VALUES (idUsers, idRooms, arrivalDate, departureDate, additionalInfo);
$$;




create function get_random_string(string_length integer, alphabet text DEFAULT 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM'::text) returns text
	language sql
as $$
WITH random_alpha AS (
    SELECT substr(alphabet,width_bucket(random(), 0, 1, (char_length(alphabet)-1)+1) + (1-1),1)
               AS char
    FROM generate_series(1,string_length)
)
SELECT array_to_string(array_agg(char), '') FROM random_alpha;
$$;

alter function get_random_string(integer, text) owner to silxielkvjqqut;

create function get_random_string(alphabet text DEFAULT 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890'::text) returns text
	language sql
as $$
WITH random_alpha AS (
    SELECT substr(alphabet, width_bucket(random(), 0, 1, (char_length(alphabet) - 1) + 1) + (1 - 1), 1)
               AS char
    FROM generate_series(1, 30)
)
SELECT array_to_string(array_agg(char), '') FROM random_alpha;
$$;


