<?php

    require_once "Routing.php";

    $path = trim($_SERVER['REQUEST_URI'], '/');
    $path = parse_url($path, PHP_URL_PATH);

    Routing::get('','DefaultController');
    Routing::get('major','DefaultController');
    Routing::post('book','BookController');
    Routing::get('confirmation','BookController');
    Routing::post('login','SecurityController');
    Routing::get('logout','SecurityController');
    Routing::post('register','SecurityController');
    Routing::post('profile','SecurityController');
    Routing::post('upload','UploadController');
    Routing::post('getProfile','SecurityController');
    Routing::post('getBook','BookController');
    Routing::post('password','SecurityController');
    Routing::post('adminLogin','SecurityController');
    Routing::post('adminPanel','SecurityController');
    Routing::post('adminPassword','SecurityController');
    Routing::post('adminListUsers','SecurityController');


    Routing::run($path);

