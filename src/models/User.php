<?php


class User
{
    private $email;
    private $password;
    private $name;
    private $surname;
    private $phone;

    private $street;
    private $street_number;
    private $postal_code;
    private $city;
    private $profile_picture;
    private $account_type;

    private $id;

    public function __construct($email, $password = null, $name, $surname, $phone = null, $street = null, $street_number = null, $postal_code = null, $city = null, $profile_picture = null, $account_type = null)
    {
        $this->email = $email;
        $this->password = $password;
        $this->name = $name;
        $this->surname = $surname;
        $this->phone = $phone;
        $this->street = $street;
        $this->street_number = $street_number;
        $this->postal_code = $postal_code;
        $this->city = $city;
        $this->profile_picture = $profile_picture;
        $this->account_type = $account_type;
    }

    public function getAccountType()
    {
        return $this->account_type;
    }

    public function setAccountType($account_type): void
    {
        $this->account_type = $account_type;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function getStreetNumber()
    {
        return $this->street_number;
    }

    public function getPostalCode()
    {
        return $this->postal_code;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getProfilePicture()
    {
        return $this->profile_picture;
    }

}