<?php

class Room
{
    private $name;
    private $price;
    private $description;
    private $photo;
    private $id;


    public function __construct($name, $price, $description, $photo, $id = null)
    {
        $this->name = $name;
        $this->price = $price;
        $this->description = $description;
        $this->photo = $photo;
        $this->id = $id;
    }


    public function getName()
    {
        return $this->name;
    }


    public function setName($name): void
    {
        $this->name = $name;
    }


    public function getPrice()
    {
        return $this->price;
    }


    public function setPrice($price): void
    {
        $this->price = $price;
    }


    public function getDescription()
    {
        return $this->description;
    }


    public function setDescription($description): void
    {
        $this->description = $description;
    }


    public function getPhoto()
    {
        return $this->photo;
    }


    public function setPhoto($photo): void
    {
        $this->photo = $photo;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

}