<?php

require_once 'AppController.php';
require_once __DIR__.'/../repository/UserRepository.php';

class UploadController extends AppController
{
    const MAX_FILE_SIZE = 1024 * 1024;
    const SUPPORTED_TYPES = ['image/png', 'image/jpeg'];
    const UPLOAD_DIRECTORY = '/../public/uploads/';

    private $messages = [];

    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }

    public function upload()
    {
        if ($this->isPost() && is_uploaded_file($_FILES['profile_picture']['tmp_name']) && $this->validate($_FILES['profile_picture']))
        {
            $this->userRepository->getUser($_COOKIE['currentUser']);

            move_uploaded_file($_FILES['profile_picture']['tmp_name'], dirname(__DIR__).self::UPLOAD_DIRECTORY.$_FILES['profile_picture']['name']);

            $this->userRepository->updateProfilePicture($_FILES['profile_picture']['name']);

            return $this->render('upload', ['messages' => ['Zdjęcie zaktualizowane.']]);
        }

        $this->render('upload', ['messages' => $this->messages]);
    }

    private function validate(array $file): bool
    {
        if ($file['size'] > self::MAX_FILE_SIZE)
        {
            $this->messages[] = 'File is too large for destination file system!';
            return false;
        }

        if (!isset($file['type']) || !in_array($file['type'], self::SUPPORTED_TYPES))
        {
            $this->messages[] = 'File type is not supported!';
            return false;
        }
        return true;
    }
}