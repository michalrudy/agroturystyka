<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/Room.php';
require_once __DIR__.'/../repository/RoomRepository.php';
require_once __DIR__.'/../repository/UserRepository.php';

class BookController extends AppController
{
    private $roomRepository;
    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->roomRepository = new RoomRepository();
        $this->userRepository = new UserRepository();
    }

    public function book()
    {
        if (!$this->isPost())
        {
            $rooms = $this->roomRepository->getRooms();

            return $this->render('book', ['rooms' => $rooms]);
        }

        $user = $this->userRepository->getUser($_COOKIE['currentUser']);

        $userId = $user->getId();
        $roomsId = $_POST['choice'];
        $arrival_date = $_POST['arrival'];
        $departure_date = $_POST['departure'];
        $additional_info = $_POST['additional_info'];


        $this->roomRepository->addBook($userId, $roomsId, $arrival_date, $departure_date, $additional_info);

        return $this->render('confirmation', ['lastBook' => $this->roomRepository->getLastBook()]);
    }

    public function confirmation()
    {
        $this->render('confirmation');
    }

    public function getBook()
    {
        header('Content-type: application/json');
        http_response_code(200);

        echo json_encode($this->roomRepository->getUserBook());
    }

}

