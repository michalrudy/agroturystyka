<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/User.php';
require_once __DIR__.'/../repository/UserRepository.php';

session_start();

class SecurityController extends AppController
{
    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }

    public function login()
    {
        if (!$this->isPost()) {
            return $this->render('login');
        }

        $email = $_POST['email'];
        $password = $_POST['password'];

        $user = $this->userRepository->getUser($email);

        $userAccountType = $this->userRepository->getAccountType($user->getEmail());

        if ($userAccountType !== 'użytkownik')
        {
            return $this->render('login', ['messages' => ['Podane dane nie należą do użytkownika zwykłego!']]);
        }

        if (!$user) {
            return $this->render('login', ['messages' => ['Nie znaleziono użytkownika!']]);
        }

        if ($user->getEmail() !== $email) {
            return $this->render('login', ['messages' => ['Użytkonik o podanym e-mail nie istnieje!']]);
        }

        if (!(password_verify($password, $user->getPassword()))) {
            return $this->render('login', ['messages' => ['Złe hasło!']]);
        }

        $this->userRepository->setCookieUser($user->getEmail());


        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/major");

    }

    public function logout()
    {
        $this->userRepository->deleteCookieUser();

        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/login");
    }

    public function register()
    {
        if (!$this->isPost())
        {
            return $this->render('register');
        }

        $email = $_POST['email'];
        $password = $_POST['password'];
        $confirmedPassword = $_POST['confirmedPassword'];
        $name = $_POST['name'];
        $surname = $_POST['surname'];
        $phone = $_POST['phone'];

        if ($password !== $confirmedPassword)
        {
            return $this->render('register', ['messages' => ['Wprowadź poprawne hasło!']]);
        }

        if (!isset($_POST['statute']))
        {
            return $this->render('register', ['messages' => ['Akceptacja regulaminu jest wymagana!']]);
        }

        $user = new User($email, password_hash($password, PASSWORD_DEFAULT), $name, $surname);
        $user->setPhone($phone);

        $this->userRepository->addUser($user);

        return $this->render('register', ['messages' => ['Rejestracja zakończona sukcesem!']]);
    }

    public function profile()
    {
        if (!$this->isPost())
        {
            return $this->render('profile');

            $this->userRepository->getUser($_COOKIE['currentUser']);
        }

        $name = $_POST['name'];
        $surname = $_POST['surname'];
        $email = $_POST['email'];
        $phone_number = $_POST['phone'];
        $street = $_POST['street'];
        $street_number = $_POST['streetNumber'];
        $postal_code = $_POST['postalCode'];
        $city = $_POST['city'];


        $this->userRepository->updateUser($name, $surname, $email, $phone_number, $street, $street_number, $postal_code, $city);

        return $this->render('profile', ['messages' => ['Zmiany zostały zapisane.']]);
    }

    public function getProfile()
    {
        header('Content-type: application/json');
        http_response_code(200);

        echo json_encode($this->userRepository->serializeToJson());
    }

    public function profilePicture()
    {
        if (!$this->isPost())
        {
            $this->userRepository->getUser($_COOKIE['currentUser']);

            return $this->render('profilePicture');
        }
    }

    public function password()
    {
        if (!$this->isPost())
        {
            return $this->render('password');

            $this->userRepository->getUser($_COOKIE['currentUser']);
        }

        $password = $_POST['pass'];
        $confirmedPassword = $_POST['confirmed_pass'];

        if ($password !== $confirmedPassword)
        {
           return $this->render('password', ['messages' => ['Wprowadź poprawne hasło!']]);
        } else {
            $this->userRepository->updateProfilePassword(password_hash($password, PASSWORD_DEFAULT));

            $this->render('password', ['messages' => ['Hasło zostało zaktualizowane.']]);
        }

    }

    // Administrator

    public function adminLogin()
    {
        if (!$this->isPost())
        {
            return $this->render('adminLogin');
        }

        $userName = $_POST['user_name'];
        $password = $_POST['password'];

        $user = $this->userRepository->getUser($userName);

        $userAccountType = $this->userRepository->getAccountType($user->getEmail());

        if ($userAccountType !== 'administrator')
        {
            return $this->render('adminLogin', ['messages' => ['Podany użytkownik nie jest administratorem!']]);
        }

        if ($user->getEmail() !== $userName) {
            return $this->render('login', ['messages' => ['Użytkownik o podanym e-mailu nie istnieje!']]);
        }

        if (!(password_verify($password, $user->getPassword()))) {
            return $this->render('adminLogin', ['messages' => ['Złe hasło!']]);
        }

        $this->userRepository->setCookieUser($user->getEmail());

        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/adminPanel");
    }

    public function adminPanel()
    {
        if (!$this->isPost())
        {
            return $this->render('adminPanel');
        }
    }

    public function adminPassword()
    {
        if (!$this->isPost())
        {
            return $this->render('adminPassword');
        }

        $password = $_POST['pass'];
        $confirmedPassword = $_POST['confirmed_pass'];

        if ($password !== $confirmedPassword)
        {
            return $this->render('adminPassword', ['messages' => ['Wprowadź poprawne hasło!']]);
        } else {
            $this->userRepository->updateProfilePassword(password_hash($password, PASSWORD_DEFAULT));

            $this->render('adminPassword', ['messages' => ['Hasło zostało zaktualizowane.']]);
        }
    }

    public function adminListUsers()
    {
        $users = $this->userRepository->getUsers();

        if (!$this->isPost()) {
            return $this->render('adminListUsers', ['users' => $users]);
        }

        $email = $_POST['email'];

        $nameAndSurname = $this->userRepository->deleteUser($email);

        $_SESSION['deletedUserName'] = $nameAndSurname['name'];
        $_SESSION['deletedUserSurname'] = $nameAndSurname['surname'];

        return $this->render('adminListUsers', ['users' => $users]);
    }
}