<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/Room.php';

class RoomRepository extends Repository
{
    private $lastBook = [];


    public function getRoom(int $id): ?Room
    {
        $stmt = $this->database->connect()->prepare('
            SELECT name, price, description, photo FROM
            (SELECT "ID_rooms", photo FROM photos p INNER JOIN rooms_photos rp on p."ID_photos" = rp."ID_photos") AS t1 INNER JOIN rooms r ON r."ID_rooms" = t1."ID_rooms" WHERE r."ID_rooms" = :id LIMIT 1
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $room = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($room == false)
        {
            return null;
        }

        return new Room(
            $room['name'],
            $room['price'],
            $room['description'],
            $room['photo']
        );
    }

    public function getRooms(): array
    {
        $newRooms = [];

        $stmt = $this->database->connect()->prepare('SELECT * FROM v_all_rooms');
        $stmt->execute();
        $rooms = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($rooms as $room)
        {
            $newRooms[] = new Room(
                $room['name'],
                $room['price'],
                $room['description'],
                $room['photo'],
                $room['ID_rooms']
            );

        }

        return $newRooms;
    }

    public function addBook($userId, $roomsId, $arrival_date, $departure_date, $additional_info)
    {
        foreach ($roomsId as $roomId)
        {
            $stmt = $this->database->connect()->prepare('
                INSERT INTO booking ("ID_users", "ID_rooms", arrival_date, departure_date, additional_info) VALUES (?, ?, ?, ?, ?)  
            ');

            $stmt->execute([
                $userId,
                $roomId,
                $arrival_date,
                $departure_date,
                $additional_info
            ]);
        }

        $this->lastBook[] = $roomsId;
        $this->lastBook[] = $arrival_date;
        $this->lastBook[] = $departure_date;
        $this->lastBook[] = $additional_info;
    }

    public function getLastBook(): array
    {
        return $this->lastBook;
    }

    public function getUserBook(): array
    {
        $stmt = $this->database->connect()->prepare('
            SELECT t2.name, t2.price, t2.description, t2.photo, t2.arrival_date, t2.departure_date, t2.additional_info FROM (SELECT t1.name, t1.price, t1.description, t1.photo, b.arrival_date, b.departure_date, b.additional_info, b."ID_users" FROM (SELECT DISTINCT ON (name) name, price, description, photo, r."ID_rooms" FROM (SELECT "ID_rooms", photo FROM photos p INNER JOIN rooms_photos rp on p."ID_photos" = rp."ID_photos") AS t1 INNER JOIN rooms r ON r."ID_rooms" = t1."ID_rooms" ORDER BY name) AS t1 INNER JOIN booking b ON b."ID_rooms" = t1."ID_rooms") AS t2 INNER JOIN users u ON u."ID_users" = t2."ID_users" WHERE u.email = :email
        ');
        $email = $_COOKIE['currentUser'];
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
