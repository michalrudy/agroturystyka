<?php

require_once __DIR__.'/../../Database.php';

class Repository
{
    protected $database;

    public function __construct()
    {
        //rozważyć wprowadzenie wrozrca projektowego singleton
        $this->database = new Database();
    }
}