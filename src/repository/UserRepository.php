<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/User.php';

session_start();

class UserRepository extends Repository
{
    public function getUser(string $email): ?User
    {
        $stmt = $this->database->connect()->prepare('
            SELECT t2.name, t2.surname, t2.email, t2.phon_number, t2.street, t2.street_number, t2.postal_code, t2.city, t2.profile_picture, t2.password, t2."ID_users", at.account_name FROM (SELECT t1.name, t1.surname, t1.email, t1.phon_number, t1.street, t1.street_number, t1.postal_code, t1.city, t1.profile_picture, t1.password, t1."ID_users", ua."ID_account_type" FROM(SELECT "name", surname, email, phon_number, street, street_number, postal_code, city, profile_picture, password, "ID_users" FROM users u LEFT JOIN users_details ud ON ud."ID_users_details" = u."ID_users_details" WHERE email = :email) t1 INNER JOIN users_account_type ua ON ua."ID_users" = t1."ID_users") t2 INNER JOIN account_type at ON at."ID_account_type" = t2."ID_account_type"
        ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($user == false)
        {
            return null;
        }

        $newUser = new User(
            $user['email'],
            $user['password'],
            $user['name'],
            $user['surname'],
            $user['phon_number'],
            $user['street'],
            $user['street_number'],
            $user['postal_code'],
            $user['city'],
            $user['profile_picture']
        );
        $newUser->setId($user['ID_users']);
        $newUser->setAccountType($user['account_name']);

        $_SESSION['name'] = $user['name'];
        $_SESSION['surname'] = $user['surname'];
        $_SESSION['email'] = $user['email'];
        $_SESSION['phon_number'] = $user['phon_number'];
        $_SESSION['street'] = $user['street'];
        $_SESSION['street_number'] = $user['street_number'];
        $_SESSION['postal_code'] = $user['postal_code'];
        $_SESSION['city'] = $user['city'];
        $_SESSION['profile_picture'] = $user['profile_picture'];

        return $newUser;
    }

    public function getUsers(): array
    {
        $newUsers = [];

        $stmt = $this->database->connect()->prepare('SELECT * FROM v_all_users');
        $stmt->execute();
        $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($users as $user)
        {
            $newUsers[] = new User(
                $user['email'],
                $user['password'],
                $user['name'],
                $user['surname'],
                $user['phon_number'],
                $user['street'],
                $user['street_number'],
                $user['postal_code'],
                $user['city'],
                $user['profile_picture'],
                $user['account_name']
            );

        }

        return $newUsers;
    }

    public function addUser(User $user)
    {
        $stmt = $this->database->connect()->prepare('
            WITH identity AS (INSERT INTO users_details (name, surname, phon_number) VALUES (?, ?, ?) RETURNING "ID_users_details") INSERT INTO users (email, password, "ID_users_details") VALUES (?, ?, (SELECT "ID_users_details" FROM identity))
        ');

        $stmt->execute([
            $user->getName(),
            $user->getSurname(),
            $user->getPhone(),
            $user->getEmail(),
            $user->getPassword()
        ]);

        $stmt = $this->database->connect()->prepare('
            SELECT "ID_users" FROM users u INNER JOIN users_details ud ON ud."ID_users_details" = u."ID_users_details" WHERE name = :name AND surname = :surname AND phon_number = :phone AND email = :email;
        ');
        $name = $user->getName();
        $surname = $user->getSurname();
        $phone = $user->getPhone();
        $email = $user->getEmail();
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':surname', $surname, PDO::PARAM_STR);
        $stmt->bindParam(':phone', $phone, PDO::PARAM_STR);
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $idUsers = $stmt->fetch(PDO::FETCH_ASSOC);

        $stmt = $this->database->connect()->prepare('
            SELECT "ID_account_type" FROM account_type WHERE account_name = :nameAccount
        ');
        $nameAcc = 'użytkownik';
        $stmt->bindParam(':nameAccount', $nameAcc, PDO::PARAM_STR);
        $stmt->execute();

        $idAccountType = $stmt->fetch(PDO::FETCH_ASSOC);

        $stmt = $this->database->connect()->prepare('
            INSERT INTO users_account_type ("ID_users", "ID_account_type") VALUES (?, ?)
        ');
        $stmt->execute([
            $idUsers['ID_users'],
            $idAccountType['ID_account_type']
        ]);
    }

    public function updateUser($name, $surname, $emailForm, $phone_number, $street, $street_number, $postal_code, $city)
    {
        $stmt = $this->database->connect()->prepare('
            SELECT "ID_users", "ID_users_details" FROM users WHERE email = :email
        ');
        $email = $_COOKIE['currentUser'];
        $stmt->bindParam(':email',$email, PDO::PARAM_STR);
        $stmt->execute();

        $idCurrentUser = $stmt->fetch(PDO::FETCH_ASSOC);


        $stmt1 = $this->database->connect()->prepare('
            UPDATE users SET email = :email WHERE "ID_users" = :idUser
        ');
        $stmt1->bindParam(':email', $emailForm, PDO::PARAM_STR);
        $stmt1->bindParam(':idUser', $idCurrentUser['ID_users'], PDO::PARAM_INT);
        $stmt1->execute();


        $stmt2 = $this->database->connect()->prepare('
            UPDATE users_details SET name = :name, surname = :surname, street = :street, street_number = :streetNumber, postal_code = :postalCode, city = :city, phon_number = :phone WHERE "ID_users_details" = :idUsersDetails
        ');
        $stmt2->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt2->bindParam(':surname', $surname, PDO::PARAM_STR);
        $stmt2->bindParam(':street', $street, PDO::PARAM_STR);
        $stmt2->bindParam(':streetNumber', $street_number, PDO::PARAM_INT);
        $stmt2->bindParam(':postalCode', $postal_code, PDO::PARAM_STR);
        $stmt2->bindParam(':city', $city, PDO::PARAM_STR);
        $stmt2->bindParam(':phone', $phone_number, PDO::PARAM_STR);
        $stmt2->bindParam(':idUsersDetails', $idCurrentUser['ID_users_details'], PDO::PARAM_INT);
        $stmt2->execute();
    }

    public function setCookieUser(string $email)
    {
        setcookie('currentUser', $email);

        $stmt = $this->database->connect()->prepare('
            UPDATE users SET enabled = true WHERE email = ?
        ');
        $stmt->execute([$email]);

        $stmt = $this->database->connect()->prepare('
            SELECT name FROM users u INNER JOIN users_details ud ON u."ID_users_details" = ud."ID_users_details" WHERE email = :email
        ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $name = $stmt->fetch(PDO::FETCH_ASSOC);

        $_SESSION['name'] = $name['name'];
    }

    public function deleteCookieUser()
    {
        setcookie('currentUser', $_COOKIE['currentUser'], time() - 1);

        $stmt = $this->database->connect()->prepare('
            UPDATE users SET enabled = false WHERE email = ?
        ');
        $stmt->execute([$_COOKIE['currentUser']]);
        session_destroy();
    }

    public function updateProfilePicture(string $fileName)
    {
        $stmt = $this->database->connect()->prepare('
            SELECT "ID_users_details" FROM users WHERE email = :email
        ');

        $stmt->bindParam(':email',$_COOKIE['currentUser'], PDO::PARAM_STR);
        $stmt->execute();

        $idUsersDetails = $stmt->fetch(PDO::FETCH_ASSOC);


        $stmt = $this->database->connect()->prepare('
            UPDATE users_details SET profile_picture = :fileName WHERE "ID_users_details" = :idUsersDetails
        ');

        $stmt->bindParam(':fileName', $fileName, PDO::PARAM_STR);
        $stmt->bindParam(':idUsersDetails', $idUsersDetails['ID_users_details'], PDO::PARAM_INT);
        $stmt->execute();

    }

    public function updateProfilePassword(string $password)
    {
        $stmt = $this->database->connect()->prepare('
            UPDATE users SET password = :password WHERE email = :email
        ');

        $stmt->bindParam(':password', $password, PDO::PARAM_STR);
        $stmt->bindParam(':email', $_COOKIE['currentUser'], PDO::PARAM_STR);
        $stmt->execute();
    }

    public function getAccountType(string $userName)
    {
        $stmt = $this->database->connect()->prepare('
            SELECT account_name FROM (SELECT "ID_account_type", email FROM users u INNER JOIN users_account_type uat ON u."ID_users" = uat."ID_users") t1 INNER JOIN account_type at ON at."ID_account_type" = t1."ID_account_type" WHERE t1.email = :email
        ');

        $stmt->bindParam(':email',$userName, PDO::PARAM_STR);
        $stmt->execute();

        $arrayAccountName = $stmt->fetch(PDO::FETCH_ASSOC);
        return $arrayAccountName['account_name'];
    }

    public function deleteUser(string $email)
    {
        $stmt = $this->database->connect()->prepare('
            SELECT "ID_users", u."ID_users_details", name, surname FROM users u INNER JOIN users_details ud on ud."ID_users_details" = u."ID_users_details" WHERE email = :email
        ');

        $stmt->bindParam(':email',$email, PDO::PARAM_STR);
        $stmt->execute();

        $identifiers = $stmt->fetch(PDO::FETCH_ASSOC);


        $stmt = $this->database->connect()->prepare('
            DELETE FROM users_account_type WHERE "ID_users" = :idUsers
        ');

        $stmt->bindParam(':idUsers',$identifiers['ID_users'], PDO::PARAM_INT);
        $stmt->execute();

        $stmt = $this->database->connect()->prepare('
            DELETE FROM users WHERE "ID_users" = :idUsers
        ');

        $stmt->bindParam(':idUsers',$identifiers['ID_users'], PDO::PARAM_INT);
        $stmt->execute();

        $stmt = $this->database->connect()->prepare('
            DELETE FROM users_details WHERE "ID_users_details" = :idUsersDetails
        ');

        $stmt->bindParam(':idUsersDetails',$identifiers['ID_users_details'], PDO::PARAM_INT);
        $stmt->execute();


        return [
            'name' => $identifiers['name'],
            'surname' => $identifiers['surname']
        ];
    }

    public function serializeToJson(): array
    {
        $user = $this->getUser($_COOKIE['currentUser']);

        return [
            'name' => $user->getName(),
            'surname' => $user->getSurname(),
            'email' => $user->getEmail(),
            'phon_number' => $user->getPhone(),
            'street' => $user->getStreet(),
            'street_number' => $user->getStreetNumber(),
            'postal_code' => $user->getPostalCode(),
            'city' => $user->getCity(),
            'profile_picture' => $user->getProfilePicture(),
            'password' => $user->getPassword()
        ];
    }

}
