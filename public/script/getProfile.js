const buttonMyData = document.querySelector('#btn-my-data');
const containerForProfile = document.querySelector('.right-bar-container');
const handlerButtonMyBooking = document.querySelector('#btn-my-booking');
const handlerButtonMyPhoto = document.querySelector('#btn-my-photo');
const handlerButtonMyPass = document.querySelector('#btn-my-pass');

buttonMyData.addEventListener('click', function(event)
{
    //event.preventDefault();

    handlerButtonMyBooking.style.backgroundImage = "linear-gradient(#fff, #fff)";
    handlerButtonMyBooking.style.color = "#c4c4c4";
    buttonMyData.style.backgroundImage = "linear-gradient(#00B300, #009900)";
    buttonMyData.style.color = "#fff";
    handlerButtonMyPhoto.style.backgroundImage = "linear-gradient(#fff, #fff)";
    handlerButtonMyPhoto.style.color = "#c4c4c4";
    handlerButtonMyPass.style.backgroundImage = "linear-gradient(#fff, #fff)";
    handlerButtonMyPass.style.color = "#c4c4c4";

    fetch('/getProfile'
    ).then(function (response){
        return response.json();
    }).then(function (data){
        containerForProfile.innerHTML = "";
        loadUser(data);
    });
});

function loadUser(data)
{
     createUser(data);
}

function createUser(user)
{
    const template = document.querySelector('.my-data-template');
    const clone = template.content.cloneNode(true);

    const photo = clone.querySelector('#handler-photo');
    if (user['profile_picture'] == null || user['profile_picture'] === "")
    {
        photo.setAttribute('alt', 'Nie ustawiono zdjęcia profilowego.')
    } else {
        photo.src = `/public/uploads/${user['profile_picture']}`;
    }
    const name = clone.querySelector('#handler-name');
    name.innerHTML = user['name'];
    const surname = clone.querySelector('#handler-surname');
    surname.innerHTML = user['surname'];
    const email = clone.querySelector('#handler-email');
    email.innerHTML = user['email'];
    const phon = clone.querySelector('#handler-phon');
    phon.innerHTML = user['phon_number'];
    const street = clone.querySelector('#handler-street');
    street.innerHTML = user['street'];
    const number = clone.querySelector('#handler-number');
    number.innerHTML = user['street_number'];
    const postal_code = clone.querySelector('#handler-postal-code');
    postal_code.innerHTML = user['postal_code'];
    const city = clone.querySelector('#handler-city');
    city.innerHTML = user['city'];

    containerForProfile.appendChild(clone);

}