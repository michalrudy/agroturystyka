const form = document.querySelector("form");
const emailInput = form.querySelector('input[name="email"]');
const submitInput = form.querySelector('input[type=submit]');


function isEmail(email) {
    return /\S+@\S+\.\S+/.test(email);
}

function markValidation(element, condition) {
    if (!condition)
    {
        element.classList.add('no-valid');
        submitInput.style.display = "none";
    } else {
        element.classList.remove('no-valid');
        submitInput.style.display = "block";
    }
}

function validateEmail() {
    setTimeout(function () {
            markValidation(emailInput, isEmail(emailInput.value));
        },
        1000
    );
}

emailInput.addEventListener('keyup', validateEmail);
