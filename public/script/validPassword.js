const form = document.querySelector("form");
const passwordInput = form.querySelector('input[name="pass"]');
const confirmedPasswordInput = form.querySelector('input[name="confirmed_pass"]');
const submitInput = form.querySelector('input[type=submit]');


function arePasswordsSame(password, confirmedPassword) {
    return password === confirmedPassword;
}

function markValidation(element, condition) {
    if (!condition)
    {
        element.classList.add('no-valid');
        submitInput.style.display = "none";
    } else {
        element.classList.remove('no-valid');
        submitInput.style.display = "inline-block";
    }
}

function validatePassword() {
    setTimeout(function () {
            const condition = arePasswordsSame(
                passwordInput.value,
                confirmedPasswordInput.value
            );
            markValidation(confirmedPasswordInput, condition);
        },
        1000
    );
}


confirmedPasswordInput.addEventListener('keyup', validatePassword);
