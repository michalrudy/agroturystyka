const buttonMyBooking = document.querySelector('#btn-my-booking');
const containerForBooking = document.querySelector('.right-bar-container');
const handlerButtonMyData = document.querySelector('#btn-my-data');


buttonMyBooking.addEventListener('click', function(event)
{
    //event.preventDefault();

    handlerButtonMyData.style.backgroundImage = "linear-gradient(#fff, #fff)";
    handlerButtonMyData.style.color = "#c4c4c4";
    buttonMyBooking.style.backgroundImage = "linear-gradient(#00B300, #009900)";
    buttonMyBooking.style.color = "#fff";
    handlerButtonMyPhoto.style.backgroundImage = "linear-gradient(#fff, #fff)";
    handlerButtonMyPhoto.style.color = "#c4c4c4";
    handlerButtonMyPass.style.backgroundImage = "linear-gradient(#fff, #fff)";
    handlerButtonMyPass.style.color = "#c4c4c4";

    fetch('/getBook'
    ).then(function (response){
        return response.json();
    }).then(function (data){
        containerForBooking.innerHTML = "";
        console.log(data)
        loadBooking(data);
    });
});

function loadBooking(data)
{
    const template = document.querySelector('.my-book-template');
    const clone = template.content.cloneNode(true);

    const handlerBodyTable1 = clone.querySelector('#handler-body-table');

    const simpleArrayFromAssocArray = {
        0: 'name',
        1: 'price',
        2: 'description',
        3: 'photo',
        4: 'arrival_date',
        5: 'departure_date',
        6: 'additional_info'
    };

    for (let i = 0; i < data.length; i++)
    {
        const tr = document.createElement("tr");

        for (let j = 0; j < 7; j++)
        {
            const td = document.createElement("td");
            if (j === 3)
            {
                const img = document.createElement("img");
                img.setAttribute('src', 'public/uploads/'+data[i][simpleArrayFromAssocArray[j]]);
                img.style.maxWidth = '15em';
                td.appendChild(img);
            } else {
                td.innerHTML = data[i][simpleArrayFromAssocArray[j]];
            }
            tr.appendChild(td);
        }
        handlerBodyTable1.appendChild(tr);
    }

    containerForBooking.appendChild(clone);
}