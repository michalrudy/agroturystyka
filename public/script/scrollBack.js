
window.addEventListener("scroll", serviceScrollUp);

function serviceScrollUp()
{
    if (document.body.scrollTop > 750 || document.documentElement.scrollTop > 750) {
        document.getElementById("scroll-up-script").style.display = 'flex';
    } else {
        document.getElementById("scroll-up-script").style.display = 'none';
    }
}