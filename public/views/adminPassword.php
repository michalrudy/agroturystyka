<?php
session_start();

if (!isset($_COOKIE['currentUser']) || (isset($_COOKIE['currentUser']) && $_COOKIE['currentUser'] != 'admin'))
{
    header("Location: adminLogin");
}
?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-Ua-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="Tutaj będzie opis strony pod SEO">
    <meta name="author" content="Michał Rudy">

    <title>Panel administracyjny - zmiana hasła</title>

    <link rel="stylesheet" href="public/css/adminStyle.css" type="text/css">
    <link rel="stylesheet" href="public/icons/css/fontello.css" type="text/css">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@400;700&display=swap" rel="stylesheet">

    <script src="public/script/validPassword.js" type="text/javascript" defer></script>

    <!--[if lt IE 9]>
    <script src="public/script/html5shiv.min.js"></script>
    <![endif]-->
</head>
<body>
<main>
    <?php include('topBar.php') ?>
    <div class="admin-panel-wrapper">
        <section class="admin-panel-left-bar">
            <ul>
                <li><a href="adminListUsers">Konta użytkowników<i class="icon-right-open"></i></a></li> <!--W formie kafelków-->
                <li><a href="adminPassword" class="admin-panel-password-checked">Zmiana hasła<i class="icon-right-open"></i></a></li>
            </ul>
        </section>
        <article class="admin-panel-password-content">
            <h4>Zmiana hasła</h4>
            <form action="adminPassword" method="POST">
                <input type="password" name="pass" placeholder="Nowe hasło"><br>
                <input type="password" name="confirmed_pass" placeholder="Powtórz hasło">
                <div class="messages">
                    <?php
                    if (isset($messages))
                    {
                        foreach ($messages as $message)
                        {
                            echo "<span style='color: red;'>$message</span>";
                        }
                    }
                    ?>
                </div>
                <input type="submit" value="Zapisz">
            </form>
        </article>
    </div>
</main>
</body>
</html>