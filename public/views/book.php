<?php
    session_start();

    if (!isset($_COOKIE['currentUser']))
    {
        header("Location: login");
    } else if (isset($_COOKIE['currentUser']) && $_COOKIE['currentUser'] == 'admin') {
        header("Location: adminPanel");
    }
?>
<!DOCTYPE html>
<html lang="pl">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-Ua-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="Tutaj będzie opis strony pod SEO">
    <meta name="author" content="Michał Rudy">

    <title>Formularz rezerwacji pokoju</title>

    <link rel="stylesheet" href="public/css/main.css" type="text/css">
    <link rel="stylesheet" href="public/icons/css/fontello.css" type="text/css">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@400;700&display=swap" rel="stylesheet">

    <script src="public/script/navbar.js" defer></script>
    <script src="public/script/scrollBack.js" defer></script>

    <!-- Analytics -->

    <!--[if lt IE 9]>
    <script src="public/script/html5shiv.min.js"></script>
    <![endif]-->

</head>
<body>
<header id="scroll-to-up">
    <nav class="main-nav main-nav-book">
        <ul class="main-menu main-menu-book">
            <li><a href="major">DOM</a></li>
            <li><a href="#">OFERTA</a></li>
            <li><a href="#">ATRAKCJE</a></li>
            <li><a href="#">GALERIA</a></li>
            <li><a href="#">KONTAKT</a></li>
            <li><a href="#">REGULAMIN</a></li>
            <?php
                if (isset($_COOKIE['currentUser'])) {
                    echo '<li id="menu-mobile-logout"><a href="logout">Wyloguj</a></li>';
                }
            ?>
            <li><a href="#"><i class="icon-user-o"></i></a>
                <ul class="main-nav-book user-options-book">
                    <?php
                    if (isset($_COOKIE['currentUser']))
                    {
                        echo '<li style="font-weight: normal;"><a href="profile">'.$_SESSION['name'].'</a></li>
                                <li style="padding: 0; font-weight: normal;"><a href="book">Zarezerwuj</a></li>
                                <li style="font-weight: normal;"><a href="logout">Wyloguj</a></li>';
                    } else {
                        echo '<li style="font-weight: normal;"><a href="login">Zaloguj</a></li>';
                    }
                    ?>
                </ul>
            </li>
        </ul>
        <div class="navbar-mobile">
            <div class="hamburger">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </div>
            <a href="#"><i class="icon-user-o navbar-mobile-icon-user"></i></a>
        </div>
    </nav>
    <?php include('scrollUp.php') ?>
</header>
<main>
    <article class="article-book" id="hause">
        <h1>Wybierz pokój</h1>

        <form action="book" method="POST" class="book-form-container">
            <?php foreach ($rooms as $room): ?>
                <label>
                    <input type="checkbox" name="choice[]" value="<?= $room->getId(); ?>" style="margin-left: 1em;">
                        <div>
                            <h3>Pokój <?= $room->getName(); ?></h3>
                            <p><?= $room->getPrice(); ?>zł/doba</p>
                            <p><?= $room->getDescription(); ?></p>
                        </div>
                        <img src="public/uploads/<?= $room->getPhoto(); ?>" alt="">
                </label>
            <?php endforeach;?>
            <div class="second-part-of-form">
                <p>Data przyjazdu</p>
                <input placeholder="Wybierz termin" type="date" name="arrival" min="<?= date('Y');?>-<?= date('m');?>-<?= date('d');?>" style="padding-left: 1em;">
                <p>Data odjazdu</p>
                <input placeholder="Wybierz termin" type="date" name="departure" min="<?= date('Y');?>-<?= date('m');?>-<?= date('d');?>"style="padding-left: 1em;">
                <p>Dodatkowe informacje:</p>
                <textarea name="additional_info" rows="8" cols="80" placeholder="W pokoju proszę o dodatkową pościel." style="resize: none; padding: 1em 1em;"></textarea>
                <input type="submit" value="Zarezerwuj">
            </div>
        </form>
    </article>
    <?php include('footer.php') ?>
</main>
</body>
</html>