<nav class="main-nav main-nav-book main-nav-profile">
    <ul class="main-menu main-menu-book main-menu-profile">
        <li><a href="major">DOM</a></li>
        <li><a href="#">OFERTA</a></li>
        <li><a href="#">ATRAKCJE</a></li>
        <li><a href="#">GALERIA</a></li>
        <li><a href="#">KONTAKT</a></li>
        <li><a href="#">REGULAMIN</a></li>
        <?php
        if (isset($_COOKIE['currentUser'])) {
            echo '<li id="menu-mobile-logout"><a href="logout">Wyloguj</a></li>';
        }
        ?>
        <li><a href="#"><i class="icon-user-o"></i></a>
            <ul class="main-nav-book user-options-book">
                <?php
                if (isset($_COOKIE['currentUser']))
                {
                    echo '<li style="font-weight: normal;"><a href="profile">'.$_SESSION['name'].'</a></li>
                                <li style="padding: 0; font-weight: normal;"><a href="book">Zarezerwuj</a></li>
                                <li style="font-weight: normal;"><a href="logout">Wyloguj</a></li>';
                } else {
                    echo '<li style="font-weight: normal;"><a href="login">Zaloguj</a></li>';
                }
                ?>
            </ul>
        </li>
    </ul>
    <div class="navbar-mobile">
        <div class="hamburger">
            <span class="bar"></span>
            <span class="bar"></span>
            <span class="bar"></span>
        </div>
        <a href="#"><i class="icon-user-o navbar-mobile-icon-user"></i></a>
    </div>
</nav>