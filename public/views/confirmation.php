<?php
session_start();

if (!isset($_COOKIE['currentUser']))
{
    header("Location: login");
}else if (isset($_COOKIE['currentUser']) && $_COOKIE['currentUser'] == 'admin') {
    header("Location: adminPanel");
} else if (isset($_COOKIE['currentUser']) && !isset($lastBook)){
    header("Location: profile");
}
?>
<!DOCTYPE html>
<html lang="pl">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-Ua-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="Tutaj będzie opis strony pod SEO">
    <meta name="author" content="Michał Rudy">

    <title>Potwierdzenie złożenia rezerwacji</title>

    <link rel="stylesheet" href="public/css/main.css" type="text/css">
    <link rel="stylesheet" href="public/icons/css/fontello.css" type="text/css">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@400;700&display=swap" rel="stylesheet">

    <script src="public/script/navbar.js" defer></script>
    <script src="public/script/scrollBack.js" defer></script>
    <script src="public/script/formHover.js" defer></script>

    <!-- Analytics -->

    <!--[if lt IE 9]>
    <script src="public/script/html5shiv.min.js"></script>
    <![endif]-->

</head>
<body>
<header id="scroll-to-up">
    <nav class="main-nav main-nav-book">
        <ul class="main-menu main-menu-book">
            <li><a href="major">DOM</a></li>
            <li><a href="#">OFERTA</a></li>
            <li><a href="#">ATRAKCJE</a></li>
            <li><a href="#">GALERIA</a></li>
            <li><a href="#">KONTAKT</a></li>
            <li><a href="#">REGULAMIN</a></li>
            <?php
            if (isset($_COOKIE['currentUser'])) {
                echo '<li id="menu-mobile-logout"><a href="logout">Wyloguj</a></li>';
            }
            ?>
            <li><a href="#"><i class="icon-user-o"></i></a>
                <ul class="main-nav-book user-options-book">
                    <?php
                    if (isset($_COOKIE['currentUser']))
                    {
                        echo '<li style="font-weight: normal;"><a href="profile">'.$_SESSION['name'].'</a></li>
                                <li style="padding: 0; font-weight: normal;"><a href="book">Zarezerwuj</a></li>
                                <li style="font-weight: normal;"><a href="logout">Wyloguj</a></li>';
                    } else {
                        echo '<li style="font-weight: normal;"><a href="login">Zaloguj</a></li>';
                    }
                    ?>
                </ul>
            </li>
        </ul>
        <div class="navbar-mobile">
            <div class="hamburger">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </div>
            <a href="#"><i class="icon-user-o navbar-mobile-icon-user"></i></a>
        </div>
    </nav>
    <?php include('scrollUp.php') ?>
</header>
<main>
    <article class="article-confirmation" id="hause">
        <h1>Potwierdzenie rezerwaji</h1>
        <div>
            <h2>Zarezerwowane pokoje</h2>
            <?php foreach ($lastBook[0] as $index): ?>
                <?php $rooms[] = (new RoomRepository)->getRoom($index);?>
            <?php endforeach;?>
            <?php foreach ($rooms as $room): ?>
                <h3>Pokój <?= $room->getName(); ?></h3>
                <p><?= $room->getPrice(); ?> zł/doba</p>
                <p><?= $room->getDescription(); ?></p>
            <?php endforeach;?>
            <p class="underline">Data przyjazdu:</p> <?= $lastBook[1]?>
            <p class="underline">Data odjazdu:</p> <?= $lastBook[2]?>
            <p class="underline">Dodatkowe informacje:</p> <?= $lastBook[3]?>
        </div>
    </article>
    <?php include('footer.php') ?>
</main>
</body>
</html>