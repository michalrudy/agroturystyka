<?php
session_start();

if (isset($_COOKIE['currentUser']) && $_COOKIE['currentUser'] == 'admin')
{
    header("Location: adminPanel");
}
?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-Ua-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="Tutaj będzie opis strony pod SEO">
    <meta name="author" content="Michał Rudy">

    <title>Zaloguj się do panelu administracyjnego</title>

    <link rel="stylesheet" href="public/css/adminStyle.css" type="text/css">
    <link rel="stylesheet" href="public/icons/css/fontello.css" type="text/css">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@400;700&display=swap" rel="stylesheet">


    <!--[if lt IE 9]>
    <script src="public/script/html5shiv.min.js"></script>
    <![endif]-->
</head>
<body>
<main>
    <section class="login-section">
        <div class="login-section-form">
            <form action="adminLogin" method="POST">
                <p>Panel administratora</p>
                <p style="margin-top: 0; color: #009900">Logowanie</p>
                <input name="user_name" type="text" placeholder="Nazwa użytkownika">
                <input name="password" type="password" placeholder="Hasło">
                <div class="messages">
                    <?php
                    if (isset($messages))
                    {
                        foreach ($messages as $message)
                        {
                            echo "<span style='color: red;'>$message</span>";
                        }
                    }
                    ?>
                </div>
                <input type="submit" value="Zaloguj się">
            </form>
        </div>
        <div class="return-to-main">
            <a href="major">
                <i class="icon-left-open"></i>
                <p>Powtór do strony głownej</p>
            </a>
        </div>
    </section>
</main>
</body>
</html>