<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="pl">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-Ua-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="Tutaj będzie opis strony pod SEO">
    <meta name="author" content="Michał Rudy">

    <title>Gospodarstwo Agroturystyczne Na Górze</title>

    <link rel="stylesheet" href="public/css/main.css" type="text/css">
    <link rel="stylesheet" href="public/icons/css/fontello.css" type="text/css">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@400;700&display=swap" rel="stylesheet">

    <script src="public/script/navbar.js" defer></script>
    <script src="public/script/scrollBack.js" defer></script>

    <!-- Analytics -->

    <!--[if lt IE 9]>
        <script src="public/script/html5shiv.min.js"></script>
    <![endif]-->

</head>
<body>

   <header class="main-header-wrapper" id="scroll-to-up">
       <img class="bg-photo-gate" alt="Brama gospodarstwo agroturystyczne Na Górze" src="public/img/strona-glowna-brama.JPG">
       <nav class="main-nav">
           <ul class="main-menu">
               <li><a href="#hause">DOM</a></li>
               <li><a href="#offer">OFERTA</a></li>
               <li><a href="#">ATRAKCJE</a></li>
               <li><a href="#">GALERIA</a></li>
               <li><a href="#">KONTAKT</a></li>
               <li><a href="#">REGULAMIN</a></li>
               <?php
                   if (isset($_COOKIE['currentUser'])) {
                       echo '<li id="menu-mobile-logout"><a href="logout">Wyloguj</a></li>';
                   }
               ?>
               <li><a href="login"><i class="icon-user-o"></i></a>
                   <ul>
                       <?php
                            if (isset($_COOKIE['currentUser']))
                            {
                                echo '<li><a href="profile">'.$_SESSION['name'].'</a></li>';
                                echo '<li style="padding: 0; font-weight: normal;" name="btn"><a href="book">Zarezerwuj</a></li>';
                                echo '<li style="padding: 0; font-weight: normal;"><a href="logout">Wyloguj</a></li>';
                            } else {
                                echo '<li style="padding: 0; font-weight: normal;"><a href="login">Zarezerwuj</a></li>';
                                echo '<li style="font-weight: normal;"><a href="login">Zaloguj</a></li>';
                            }
                       ?>
                   </ul>
               </li>
           </ul>
           <div class="navbar-mobile">
               <div class="hamburger">
                   <span class="bar"></span>
                   <span class="bar"></span>
                   <span class="bar"></span>
               </div>
               <?php
                   if (!isset($_COOKIE['currentUser']))
                   {
                       echo '<a class="navbar-mobile-book" href="login">Zarezerwuj</a>';
                   } else {
                       echo '<a class="navbar-mobile-book" href="book">Zarezerwuj</a>';
                   }
               ?>
               <a href="login"><i class="icon-user-o navbar-mobile-icon-user"></i></a>
           </div>
       </nav>

       <div class="logo-container">
           <img class="logo-style" src="public/img/logo.png" alt="Logo gospodarstwo agroturystyczne Na Górze">
           <p class="logo-text-first-line">Agroturystyka Na Górze</p>
           <p class="logo-text-second-line">Obrocz</p>
       </div>

       <a class="style-icon-down" href="#hause"><i class="icon-down-open"></i></a>
       <?php include('scrollUp.php') ?>
   </header>
   <main>
       <article class="container-about-hause" id="hause">
           <img class="img-about-hause" src="public/img/lrot-miejsce-marki-roztocze.JPG" alt="LROT miejsce marki roztocze">
           <div class="wrapper-about-hause">
               <header><h1 class="header-about-hause">Dom</h1></header>
               <p class="text-about-hause">Drewniany, urządzony w wiejskim stylu dom emanuje ciepłem domowego ogniska i zachęca gości do odkrywania dawnej polskiej wsi. W zakamarkach domu można bowiem odnaleźć narzędzia i przedmioty takie jak: magielnica, niecka, maselnica, wiklinowe kosze, gliniane garnki etc., związane z uprawą roli, hodowlą zwierząt, dawnym rzemiosłem i po prostu wiejskim gospodarstwem naszych dziadków i pradziadków.</p>
           </div>
       </article>
       <article class="container-offer" id="offer">
           <header class="header-offer"><h1 style="font-weight: normal">Oferta</h1></header>
           <p class="text-offer">Agroturystyka „Na Górze” oferuje swoim gościom noclegi w drewnianych, urządzonych w stylu wiejskiej chaty, domach, przeznaczonych wyłącznie na użytek gości - aby czuli się swojsko i swobodnie. Dom, w całości wykonany przez gospodarza oferuje gościom 2 pokoje dwuosobowe, 2 pokoje trzyosobowe i 1 sześcioosobowy. Wszystkie urządzone w drewnie pokoje, ozdobione haftowanymi makatkami i suszonymi kwiatami oraz obrazami wyposażone są w drewniane łóżka i szafy wykonane własnoręcznie przez gospodarza.</p>
           <div class="wrapper-offer-overline-green"></div>
           <div class="wrapper-offer-images">
               <img class="img-offer-home" src="public/img/doma-dla-gosci.JPG">
                <img class="img-offer-flowers" src="public/img/malwy-na-gorze.JPG">
                <img class="img-offer-crucifix" src="public/img/krzyz-pole.JPG">
                <div class="wrapper-offer-overline-white"></div>
           </div>
       </article>
   </main>
</body>
</html>