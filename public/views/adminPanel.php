<?php
session_start();

if (!isset($_COOKIE['currentUser']) || (isset($_COOKIE['currentUser']) && $_COOKIE['currentUser'] != 'admin'))
{
    header("Location: adminLogin");
}
?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-Ua-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="Tutaj będzie opis strony pod SEO">
    <meta name="author" content="Michał Rudy">

    <title>Panel administracyjny - <?=$_COOKIE['currentUser']?></title>

    <link rel="stylesheet" href="public/css/adminStyle.css" type="text/css">
    <link rel="stylesheet" href="public/icons/css/fontello.css" type="text/css">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@400;700&display=swap" rel="stylesheet">


    <!--[if lt IE 9]>
    <script src="public/script/html5shiv.min.js"></script>
    <![endif]-->
</head>
<body>
<main>
    <?php include('topBar.php') ?>
    <div class="admin-panel-wrapper">
        <section class="admin-panel-left-bar">
            <ul>
                <li><a href="adminListUsers">Konta użytkowników<i class="icon-right-open"></i></a></li> <!--W formie kafelków-->
                <li><a href="adminPassword">Zmiana hasła<i class="icon-right-open"></i></a></li>
            </ul>
        </section>
        <article class="admin-panel-content">
            <h2>Witaj w panelu administracyjnym</h2>
            <h4>Wybierz opcje z menu</h4>
        </article>
    </div>
</main>
</body>
</html>