<?php
session_start();

if (!isset($_COOKIE['currentUser']))
{
    header("Location: login");
}else if (isset($_COOKIE['currentUser']) && $_COOKIE['currentUser'] == 'admin') {
    header("Location: adminPanel");
}
?>
<!DOCTYPE html>
<html lang="pl">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-Ua-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="Tutaj będzie opis strony pod SEO">
    <meta name="author" content="Michał Rudy">

    <title>Zmiana zdjęcia profilowego użytkownika <?=$_SESSION['name']?></title>

    <link rel="stylesheet" href="public/css/main.css" type="text/css">
    <link rel="stylesheet" href="public/icons/css/fontello.css" type="text/css">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@400;700&display=swap" rel="stylesheet">

    <script src="public/script/navbar.js" type="text/javascript" defer></script>
    <script src="public/script/scrollBack.js" type="text/javascript" defer></script>
    <script src="./public/script/getProfile.js" type="text/javascript" defer></script>
    <script src="./public/script/getBooking.js" type="text/javascript" defer></script>

    <!-- Analytics -->

    <!--[if lt IE 9]>
    <script src="public/script/html5shiv.min.js"></script>
    <![endif]-->

</head>
<body>
<header id="scroll-to-up">
    <?php include('nav.php') ?>
    <?php include('scrollUp.php') ?>
</header>
<main>
    <article class="article-profile" id="hause">
        <section class="left-bar">
            <ul>
                <li id="btn-my-data"><span style="padding-left: 1em;">Moje dane</span><i class="icon-right-open"></i></li>
                <li id="btn-my-booking"><span style="padding-left: 1em;">Rezerwacje</span><i class="icon-right-open"></i></li>
                <li><a href="upload" id="btn-my-photo" class="btn-photo-style" style="color: #fff;"><span style="padding-left: 1em;">Zdjęcie profilowe</span><i class="icon-right-open"></i></a></li>
                <li><a href="password" id="btn-my-pass"><span style="padding-left: 1em;">Hasło</span><i class="icon-right-open"></i></a></li>
            </ul>
        </section>
        <section class="right-bar">
            <div class="right-bar-container">
                <h4>Zdjęcie profilowe</h4>
                <form action="upload" method="POST" ENCTYPE="multipart/form-data" class="edit-form form-profile-picture">
                    <label>
                        <input type="file" name="profile_picture">
                        <?php
                            if ($_SESSION['profile_picture'] == null || $_SESSION['profile_picture'] == '')
                            {
                                echo '<p class="echo-hover">Nie ustawiono zdjęcia profilowego. Kliknij aby przesłać nowe zdjęcie. Następnie kliknij zapisz.</p>';
                            } else {
                                echo '<img src="/public/uploads/'.$_SESSION['profile_picture'].'" title="Kliknij aby dodać nowe zdjęcie.">';
                            }
                        ?>
                    </label>
                    <div class="messages">
                        <?php
                        if (isset($messages))
                        {
                            foreach ($messages as $message)
                            {
                                echo "<span style='color: red;'>$message</span>";
                            }
                        }
                        ?>
                    </div>
                    <input type="submit" value="Zapisz">
                </form>
            </div>
        </section>
    </article>
    <?php include('footer.php') ?>
</main>
</body>
<template class="my-data-template">
    <img src="" alt="" id="handler-photo">
    <p class="margin-template">Imię</p><p id="handler-name" class="results-templates"></p>
    <p class="margin-template">Nazwisko</p><p id="handler-surname" class="results-templates"></p>
    <p class="margin-template">E-mail</p><p id="handler-email" class="results-templates"></p>
    <p class="margin-template">Numer telefonu</p><p id="handler-phon" class="results-templates"></p>
    <p class="margin-template">Ulica</p><p id="handler-street" class="results-templates"></p>
    <p class="margin-template">Numer domu/mieszkania</p><p id="handler-number" class="results-templates"></p>
    <p class="margin-template">Kod pocztowy</p><p id="handler-postal-code" class="results-templates"></p>
    <p class="margin-template">Miasto</p><p id="handler-city" class="results-templates" style="margin-bottom: 5em;"></p>
</template>
<template class="my-book-template">
    <table>
        <thead>
            <tr>
                <th>Nazwa</th><th>Cena</th><th>Opis</th><th>Zdjęcie</th><th>Data przyjazdu</th><th>Data odjazdu</th><th>Dodatkowe informacje</th>
            </tr>
        </thead>
        <tbody id="handler-body-table">
        </tbody>
    </table>
</template>
</html>

