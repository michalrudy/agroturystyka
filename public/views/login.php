<?php
    session_start();

    if (isset($_COOKIE['currentUser']) && $_COOKIE['currentUser'] == 'admin')
    {
        header("Location: adminPanel");
    }else if (isset($_COOKIE['currentUser'])) {
        header("Location: profile");
    }

?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-Ua-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="Tutaj będzie opis strony pod SEO">
    <meta name="author" content="Michał Rudy">

    <title>Zaloguj się na konto</title>

    <link rel="stylesheet" href="public/css/forms.css" type="text/css">
    <link rel="stylesheet" href="public/icons/css/fontello.css" type="text/css">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@400;700&display=swap" rel="stylesheet">

    <script src="public/script/primary.js" defer></script>

    <!--[if lt IE 9]>
    <script src="public/script/html5shiv.min.js"></script>
    <![endif]-->
</head>
<body>
    <main>
        <section class="container">
            <img class="container-background" src="public/img/tlo-zaloguj.JPG" alt="Tło gospodarstwo agroturystyczne Na Górze">
            <a href="major" class="logo-style"><img src="public/img/logo.png" alt="Logo gospodarstwo agroturystyczne Na Górze"></a>
            <div class="form-style">
                <form action="login" method="POST">
                    <p>Logowanie</p>
                    <input name="email" type="text" placeholder="Email" style="padding-left: 1em;">
                    <input name="password" type="password" placeholder="Hasło" style="padding-left: 1em;">
                    <div class="messages">
                        <?php
                            if (isset($messages))
                            {
                                foreach ($messages as $message)
                                {
                                    echo "<span style='color: red;'>$message</span>";
                                }
                            }
                        ?>
                    </div>
                    <input type="submit" value="Zaloguj się">
                    <div class="form-style-headers">
                        <a href="register" class="registerLink">Zarejestrój się</a>
                        <a href="adminLogin" class="registerLink">Panel administratora</a>
                    </div>
                </form>
            </div>
            <div class="icon-return">
                <a href="major">
                    <i class="icon-left-open"></i>
                    <p>Powtór do strony głownej</p>
                </a>
            </div>
        </section>
    </main>
</body>
</html>