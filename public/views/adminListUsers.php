<?php
session_start();

if (!isset($_COOKIE['currentUser']) || (isset($_COOKIE['currentUser']) && $_COOKIE['currentUser'] != 'admin'))
{
    header("Location: adminLogin");
}
?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-Ua-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="Tutaj będzie opis strony pod SEO">
    <meta name="author" content="Michał Rudy">

    <title>Panel administracyjny - lista kont</title>

    <link rel="stylesheet" href="public/css/adminStyle.css" type="text/css">
    <link rel="stylesheet" href="public/icons/css/fontello.css" type="text/css">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@400;700&display=swap" rel="stylesheet">

    <script src="public/script/scrollBack.js" defer></script>

    <!--[if lt IE 9]>
    <script src="public/script/html5shiv.min.js"></script>
    <![endif]-->
</head>
<body>
<main>
    <?php include('topBar.php') ?>
    <div class="admin-panel-wrapper">
        <section class="admin-panel-left-bar">
            <ul>
                <li><a href="adminListUsers" class="admin-panel-password-checked">Konta użytkowników<i class="icon-right-open"></i></a></li>
                <li><a href="adminPassword">Zmiana hasła<i class="icon-right-open"></i></a></li>
            </ul>
        </section>
        <article class="admin-panel-list-users-content">
            <!--Tu bedzie info, że uzytkownik został usunięty-->
            <?php
            if (isset($_SESSION['deletedUserName']) && isset($_SESSION['deletedUserSurname']))
            {
                echo "<div class='response-from-controller'>Użytkownik ".$_SESSION['deletedUserName']." ".$_SESSION['deletedUserSurname']." został usuniety!</div>";
                unset($_SESSION['deletedUserName']);
                unset($_SESSION['deletedUserSurname']);
            }
            ?>
            <?php foreach ($users as $user): ?>
                <div class="user-box">
                    <div class="user-box-div-img">
                        <img src="public/uploads/<?=$user->getProfilePicture();?>" alt="">
                    </div>
                    <div class="user-box-content">
                        <div class="line-top font-style"><p><?= $user->getName();?></p><p><?= $user->getSurname();?></p></div>
                        <div class="line font-style"><p>Typ konta</p><p><?= $user->getAccountType();?></p></div>
                        <div><p class="font-style">E-mail</p><p><?= $user->getEmail();?></p></div>
                        <div><p class="font-style">Telefon</p><p><?= $user->getPhone();?></p></div>
                        <div><p class="font-style">Ulica</p><p><?= $user->getStreet();?></p></div>
                        <div><p class="font-style">Numer domu/mieszkania</p><p><?= $user->getStreetNumber();?></p></div>
                        <div><p class="font-style">Kod pocztowy</p><p><?= $user->getPostalCode();?></p></div>
                        <div><p class="font-style">Miasto</p><p><?= $user->getCity();?></p></div>
                        <form action="adminListUsers" method="POST">
                            <input type="text" name="email" value="<?= $user->getEmail();?>" style="display: none;">
                            <button type="submit">Usuń konto</button>
                        </form>
                    </div>
                </div>
            <?php endforeach;?>
            <?php include('scrollUp.php') ?>
        </article>
    </div>
</main>
</body>
</html>